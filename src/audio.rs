use rodio::{Decoder, OutputStream, OutputStreamHandle, Sample, Sink, Source};

use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{mpsc, Arc, Mutex};
use std::thread::{self, JoinHandle};
use std::time::Duration;

struct SourceWithFn<S, F>(S, Option<F>);

impl<S, F> Iterator for SourceWithFn<S, F>
where
    S: Source,
    S::Item: Sample,
    F: FnOnce(),
{
    type Item = S::Item;
    #[inline]
    fn next(&mut self) -> Option<S::Item> {
        if let Some(n) = self.0.next() {
            Some(n)
        } else {
            self.1.take().unwrap()();
            None
        }
    }
    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl<S, F> Source for SourceWithFn<S, F>
where
    S: Source,
    S::Item: Sample,
    F: FnOnce(),
{
    #[inline]
    fn current_frame_len(&self) -> Option<usize> {
        self.0.current_frame_len()
    }
    #[inline]
    fn channels(&self) -> u16 {
        self.0.channels()
    }
    #[inline]
    fn sample_rate(&self) -> u32 {
        self.0.sample_rate()
    }
    #[inline]
    fn total_duration(&self) -> Option<Duration> {
        self.0.total_duration()
    }
}

impl<S, F> SourceWithFn<S, F>
where
    S: Source,
    S::Item: Sample,
    F: FnOnce(),
{
    fn wrap(source: S, f: F) -> Self {
        Self(source, Some(f))
    }
}

pub struct Playback {
    pub sink: Arc<Sink>,
    pub sender: mpsc::Sender<()>,
    pub receiver: mpsc::Receiver<()>,
    pub seconds_played: Arc<Mutex<u64>>,
    pub stop_flag: Arc<AtomicBool>,
    pub thread: Option<JoinHandle<()>>,
    _stream: OutputStream,
    _handle: OutputStreamHandle,
}

impl Playback {
    pub fn new() -> Self {
        let (stream, handle) = OutputStream::try_default().unwrap();
        let sink = Sink::try_new(&handle).unwrap();
        let (sender, receiver) = mpsc::channel::<()>();

        Self {
            _stream: stream,
            _handle: handle,
            sink: Arc::new(sink),
            sender,
            receiver,
            seconds_played: Arc::new(Mutex::new(0)),
            stop_flag: Arc::new(AtomicBool::new(false)),
            thread: None,
        }
    }

    pub fn play(&mut self, path: &PathBuf, duration: u64) {
        if let Some(thread) = self.thread.take() {
            let _ = thread.join();
        }

        *self.seconds_played.lock().unwrap() = 0;

        let file = BufReader::new(File::open(path).unwrap());
        let source = Decoder::new(file).unwrap();

        let sender = self.sender.clone();
        let source = SourceWithFn::wrap(source, move || sender.send(()).unwrap());

        if !self.sink.empty() {
            self.sink.clear();
            self.stop_flag.store(true, Ordering::Relaxed);
        }

        self.sink.append(source);
        self.sink.play();

        let sink = self.sink.clone();
        let stop_flag = self.stop_flag.clone();
        let seconds_played = self.seconds_played.clone();

        let thread = thread::spawn(move || loop {
            if stop_flag.load(Ordering::Relaxed) {
                break;
            }
            if *seconds_played.lock().unwrap() >= duration {
                break;
            }
            if !sink.is_paused() {
                thread::sleep(Duration::from_secs(1));
                *seconds_played.lock().unwrap() += 1;
            }
        });

        self.thread = Some(thread);
    }

    pub fn toggle_play(&self) {
        if self.sink.is_paused() {
            self.sink.play();
        } else {
            self.sink.pause();
        }
    }

    pub fn set_volume(&self, value: f32) {
        let volume = (self.sink.volume() + value).clamp(0.0, 1.0);
        self.sink.set_volume(volume);
    }

    pub fn track_progress(&self, duration: f64) -> f64 {
        let seconds_played = self.seconds_played.clone();
        let res = (*seconds_played.lock().unwrap() as f64 / duration).clamp(0.0, 1.0);
        res
    }
}
