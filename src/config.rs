use std::fs;
use std::path::PathBuf;

use directories::UserDirs;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Config {
    pub music_dir: PathBuf,
}

impl Config {
    pub fn new() -> Self {
        let path = Config::get_config_path().unwrap();
        let content = fs::read_to_string(path).unwrap();
        let config: Config = toml::from_str(content.as_str()).unwrap();
        config
    }

    fn get_config_path() -> Option<PathBuf> {
        let path = if let Some(user_dirs) = UserDirs::new() {
            let home_dir = user_dirs.home_dir();
            home_dir.join(".config").join("sonus").join("config.toml")
        } else {
            return None;
        };

        Some(path)
    }
}
