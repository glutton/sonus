#![warn(clippy::all, clippy::pedantic)]

mod audio;
mod config;
mod tracks_reader;
mod ui;

use crossterm::event::{self, KeyCode, KeyEventKind};
use ratatui::backend::CrosstermBackend;
use ratatui::widgets::TableState;
use ratatui::Terminal;

use std::io;
use std::io::Stdout;
use std::time::Duration;

use audio::Playback;
use config::Config;
use tracks_reader::{get_tracks, Track};

struct App {
    exit: bool,
    state: TableState,
    tracks: Vec<Track>,
    playback: Playback,
    current_track: usize,
    config: Config,
}

impl App {
    fn new() -> Self {
        Self {
            exit: false,
            state: TableState::new().with_selected(Some(0)),
            tracks: Vec::new(),
            current_track: 0,
            playback: Playback::new(),
            config: Config::new(),
        }
    }

    fn run(&mut self, mut terminal: Terminal<CrosstermBackend<Stdout>>) -> io::Result<()> {
        self.tracks = get_tracks(&self.config.music_dir)?;

        while !self.exit {
            terminal.draw(|frame| ui::draw(frame, self))?;

            if event::poll(Duration::from_millis(16))? {
                self.handle_events()?;
            }

            while self.playback.receiver.try_recv().is_ok() {
                self.play(self.current_track + 1);
            }
        }

        Ok(())
    }

    fn selected_row(&self) -> usize {
        self.state.selected().unwrap()
    }

    fn exit(&mut self) {
        self.exit = true;
    }

    fn play(&mut self, track_index: usize) {
        let track = &self.tracks[track_index];
        self.current_track = track_index;
        self.playback.play(&track.path, track.duration.as_secs());
    }

    fn handle_events(&mut self) -> io::Result<()> {
        match event::read()? {
            // it's important to check that the event is a key press event as
            // crossterm also emits key release and repeat events on Windows.
            event::Event::Key(key_event) if key_event.kind == KeyEventKind::Press => {
                self.handle_key_event(key_event);
            }
            _ => {}
        };
        Ok(())
    }

    fn handle_key_event(&mut self, key_event: event::KeyEvent) {
        match key_event.code {
            KeyCode::Char('q') => self.exit(),
            KeyCode::Char('j') | KeyCode::Down => self.state.select_next(),
            KeyCode::Char('k') | KeyCode::Up => self.state.select_previous(),
            KeyCode::Enter => self.play(self.selected_row()),
            KeyCode::Char(' ') => self.playback.toggle_play(),
            KeyCode::Char('+') => self.playback.set_volume(0.05),
            KeyCode::Char('-') => self.playback.set_volume(-0.05),
            _ => {}
        }
    }
}

fn main() -> io::Result<()> {
    let terminal = ratatui::init();
    let app_result = App::new().run(terminal);
    ratatui::restore();
    app_result
}
