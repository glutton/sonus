use crate::{App, Track};
use ratatui::{
    prelude::*,
    symbols,
    widgets::{Block, BorderType, Borders, LineGauge, Padding, Paragraph, Row, Table, Wrap},
};
use std::time::Duration;

const BACKGROUND_PRIMARY: Color = Color::Rgb(25, 25, 25);
const FIERY_ORANGE: Color = Color::Rgb(180, 85, 22);
const HIGHLIGHT_FOREGROUND: Color = Color::Rgb(27, 25, 26);
const HIGHLIGHT_BACKGROUND: Color = Color::Rgb(238, 232, 213);
const HEADER_BACKGROUND: Color = Color::Rgb(50, 50, 50);

fn format_duration(duration: Duration) -> String {
    let seconds = duration.as_secs() % 60;
    let minutes = (duration.as_secs() / 60) % 60;
    format!("{minutes}:{seconds:02}")
}

fn rounded_block<'a>() -> Block<'a> {
    return Block::bordered().border_type(BorderType::Rounded);
}

fn table(tracks: &[Track], current_track: usize, selected_row: usize) -> Table<'_> {
    let widths = [
        Constraint::Max(6),
        Constraint::Fill(1),
        Constraint::Percentage(10),
        Constraint::Percentage(25),
        Constraint::Percentage(25),
    ];
    let block = Block::bordered().border_type(BorderType::Rounded);

    let header = Row::new(["#", "Title", "Duration", "Artist", "Album"])
        .style(Style::new().bold().bg(HEADER_BACKGROUND));

    let highlight_foreground = if selected_row == current_track {
        FIERY_ORANGE
    } else {
        HIGHLIGHT_FOREGROUND
    };

    let highlight_style = Style::new()
        .bg(HIGHLIGHT_BACKGROUND)
        .fg(highlight_foreground);

    let get_cells = |(index, track): (usize, &Track)| {
        let cells = [
            index.to_string(),
            track.title.clone(),
            format_duration(track.duration),
            track.artist.clone(),
            track.album.clone(),
        ];

        let style = if index == current_track {
            Style::new().fg(FIERY_ORANGE)
        } else {
            Style::new()
        };

        Row::new(cells).style(style)
    };

    tracks
        .iter()
        .enumerate()
        .map(get_cells)
        .collect::<Table>()
        .widths(widths)
        .block(block)
        .header(header)
        .highlight_symbol(">")
        .highlight_style(highlight_style)
        .style(Style::new().bg(BACKGROUND_PRIMARY))
}

pub fn draw(frame: &mut Frame, app: &mut App) {
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(100), Constraint::Min(4)])
        .split(frame.area());

    let sub_layout = Layout::default()
        .direction(Direction::Horizontal)
        .constraints(Constraint::from_percentages([20, 60, 20]))
        .split(layout[1]);

    let current_track = &app.tracks[app.current_track];

    let currently_playing = format!(
        "{} - {} - {}",
        current_track.title, current_track.artist, current_track.album,
    );

    let text = Paragraph::new(currently_playing)
        .block(
            rounded_block()
                .padding(Padding::horizontal(1))
                .borders(Borders::TOP | Borders::LEFT | Borders::BOTTOM),
        )
        .style(Style::new().bg(BACKGROUND_PRIMARY))
        .wrap(Wrap { trim: true });

    let volume = Paragraph::new(format!("Volume: {}", app.playback.sink.volume())).block(
        rounded_block()
            .borders(Borders::TOP | Borders::BOTTOM | Borders::RIGHT)
            .padding(Padding::horizontal(1)),
    );

    let progress = app
        .playback
        .track_progress(current_track.duration.as_secs_f64());

    let line = LineGauge::default()
        .block(rounded_block().borders(Borders::TOP | Borders::BOTTOM))
        .label(format!(
            "{}/{}",
            format_duration(Duration::new(
                *app.playback.seconds_played.clone().lock().unwrap(),
                0
            )),
            format_duration(current_track.duration)
        ))
        .filled_style(
            Style::default()
                .fg(FIERY_ORANGE)
                .bg(Color::Black)
                .add_modifier(Modifier::BOLD),
        )
        .line_set(symbols::line::THICK)
        .ratio(progress);

    let table = table(&app.tracks, app.current_track, app.selected_row());

    frame.render_stateful_widget(table, layout[0], &mut app.state);
    frame.render_widget(text, sub_layout[0]);
    frame.render_widget(line, sub_layout[1]);
    frame.render_widget(volume, sub_layout[2]);
}
