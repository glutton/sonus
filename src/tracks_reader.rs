use std::time::Duration;

use lofty::{read_from_path, Accessor, AudioFile, TaggedFileExt};
use std::ffi::OsStr;
use std::path::{Path, PathBuf};
use std::{fs, io};

const SUPPORTED_CODECS: [&str; 5] = ["mp3", "mp4", "wav", "ogg", "flac"];

pub struct Track {
    pub path: PathBuf,
    pub title: String,
    pub artist: String,
    pub album: String,
    pub duration: Duration,
}

pub fn get_tracks(path: &PathBuf) -> Result<Vec<Track>, io::Error> {
    let entries = get_music_entries(path)?;
    Ok(collect_tracks(&entries))
}

fn collect_tracks(paths: &[PathBuf]) -> Vec<Track> {
    paths
        .iter()
        .map(|path| {
            let tagged_file = read_from_path(path).expect("should be a valid path");

            let tag = match tagged_file.primary_tag() {
                Some(primary_tag) => primary_tag,
                None => tagged_file.first_tag().expect("should contain tags"),
            };

            let title = tag.title().as_deref().unwrap_or("None").to_string();
            let artist = tag.artist().as_deref().unwrap_or("None").to_string();
            let album = tag.album().as_deref().unwrap_or("None").to_string();
            let duration = tagged_file.properties().duration();

            Track {
                path: path.clone(),
                title,
                artist,
                album,
                duration,
            }
        })
        .collect::<Vec<Track>>()
}

fn is_supported_codec(file: &Path) -> bool {
    match file.extension().and_then(OsStr::to_str) {
        Some(extension) => SUPPORTED_CODECS.contains(&extension),
        None => false,
    }
}

fn get_music_entries(path: &PathBuf) -> io::Result<Vec<PathBuf>> {
    let mut music_entries = Vec::<PathBuf>::new();

    for entry in (fs::read_dir(path)?).flatten() {
        let path = entry.path();

        if path.is_dir() {
            let entries = get_music_entries(&path)?;
            music_entries.extend_from_slice(&entries);
        } else if is_supported_codec(&path) {
            music_entries.push(path);
        }
    }

    music_entries.sort();

    Ok(music_entries)
}
